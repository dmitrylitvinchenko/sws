var util = require('util');

/**
 * Message protocol parser exception
 * @constructor
 * @extends Error
 */
function MessageParseError() { Error.apply(this, arguments); }
util.inherits(MessageParseError, Error);

/**
 * Not implemented expection
 * @constructor
 * @extends Error
 */
function NotImplemented() { Error.apply(this, arguments); }
util.inherits(NotImplemented, Error);


module.exports = {
    MessageParseError: MessageParseError
};