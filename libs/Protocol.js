
var PROTOCOL = module.exports = {};

PROTOCOL.ERROR = {};
PROTOCOL.ERROR.GROUP_NOT_EXISTS     = 0xa0;
PROTOCOL.ERROR.GROUP_ALREADY_EXISTS = 0xa1;
PROTOCOL.ERROR.GROUP_NAME_INVALID   = 0xa2;

PROTOCOL.json = {
    v1: require('./protocols/json.v1')
};
