var Exceptions = require('../Exceptions');

module.exports = {
    /**
     * Protocol version
     * @type {String}
     */
    version: 'json v1',

    /**
     * Message parse method.
     * @param {SWSMessage} message
     * @throws Exceptions.MessageParseError
     */
    parse: function(message) {
        throw new Exceptions.MessageParseError();
    }
};