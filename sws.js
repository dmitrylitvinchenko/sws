
var sws = {
    Protocol: require('./libs/Protocol'),
    protocols: require('./libs/protocols'),
    Server: require('./libs/Server')
    //Client: require('./libs/Client')
};

module.exports = sws;