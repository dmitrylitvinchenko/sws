var assert = require('assert');

var protocol = require('./../../sws').protocols.json.v1;

describe('SWS Protocol json v1', function() {

    it('should be correct version', function() {
        assert.equal(protocol.version, 'json v1');
    });

});