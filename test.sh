#!/bin/bash

scenario=$1
echo $scenario

if [ $scenario == 'start' ] ; then
    node ./node_modules/mocha/bin/mocha \
        --recursive \
        tests/unit
elif [ $scenario == 'run' ] ; then
    node ./node_modules/mocha/bin/mocha \
        --recursive \
        --watch \
        tests/unit
fi